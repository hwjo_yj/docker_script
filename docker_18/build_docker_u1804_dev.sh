#!/bin/bash

sudo docker build --tag ubuntu:yj18 .

sudo docker container stop yjbuild18
sudo docker container rm yjbuild18
sudo docker run -it -d --name yjbuild18 ubuntu:yj18

sudo docker container exec -it yjbuild18 sh -c 'rm /opt/yujin_ws/gauntlet_2/ -rf'
sudo docker container exec -it yjbuild18 sh -c 'mkdir /opt/yujin_ws/gauntlet_2/build/ -p'
sudo docker container exec -it yjbuild18 sh -c 'rm /opt/yujin_ws/shield_2/ -rf'
sudo docker container exec -it yjbuild18 sh -c 'mkdir /opt/yujin_ws/shield_2/build/ -p'
sudo docker container exec -it yjbuild18 sh -c 'rm /opt/yujin_ws/sword_2/ -rf'
sudo docker container exec -it yjbuild18 sh -c 'mkdir /opt/yujin_ws/sword_2/build/ -p'

echo 'remove old gauntlet, shield and sword in working directory...'
sudo rm ./gauntlet_2 -rf
sudo rm ./shield_2 -rf
sudo rm ./sword_2 -rf
sudo rm *.deb

echo 'copy gauntlet, shield and sword from host into docker image...'
sudo cp -r /opt/yujin_ws/gauntlet_2/ .
sudo cp -r /opt/yujin_ws/shield_2/ .
sudo cp -r /opt/yujin_ws/sword_2/ .

sudo rm ./gauntlet_2/build/ -rf
sudo rm ./shield_2/build/ -rf
sudo rm ./sword_2/build/ -rf

sudo docker cp gauntlet_2/ yjbuild18:/opt/yujin_ws/
sudo docker cp shield_2/ yjbuild18:/opt/yujin_ws/
sudo docker cp sword_2/ yjbuild18:/opt/yujin_ws/

echo 'remove temporal gauntlet, shield and sword in working directory...'
sudo rm ./gauntlet_2 -rf
sudo rm ./shield_2 -rf
sudo rm ./sword_2 -rf

#build gauntlet for shield build and package
sudo docker container exec -it yjbuild18 sh -c '\
	cd /opt/yujin_ws/gauntlet_2/build/ \
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=FALSE \
	&& make -j12 install \
	&& make package'

#build shield for sword build and package
sudo docker container exec -it yjbuild18 sh -c '\
	ldconfig\
	&& cd /opt/yujin_ws/shield_2/build/\
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=FALSE \
	&& make -j12 install \
	&& make package'

#build sword and package
sudo docker container exec -it yjbuild18 sh -c '\
	ldconfig\
	&& cd /opt/yujin_ws/sword_2/build/\
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=FALSE \
	&& make -j12 install\
	&& make package'

LIB_GAUNTLET_DEB=($(sudo docker container exec -it yjbuild18 sh -c 'ls /opt/yujin_ws/gauntlet_2/build/*.deb'| sed -e 's/\r//g'))
LIB_SHIELD_DEB=($(sudo docker container exec -it yjbuild18 sh -c 'ls /opt/yujin_ws/shield_2/build/*.deb'| sed -e 's/\r//g'))
LIB_SWORD_DEB=($(sudo docker container exec -it yjbuild18 sh -c 'ls /opt/yujin_ws/sword_2/build/*.deb'| sed -e 's/\r//g'))

sudo docker cp yjbuild18:${LIB_GAUNTLET_DEB} .
sudo docker cp yjbuild18:${LIB_SHIELD_DEB} .
sudo docker cp yjbuild18:${LIB_SWORD_DEB} .

sudo chown -c ${USER} *.deb
sudo chmod 644 *.deb

sudo docker container kill yjbuild18
