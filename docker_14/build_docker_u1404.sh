#!/bin/bash

sudo docker build --tag ubuntu:yj14 .

sudo docker container stop yjbuild14
sudo docker container rm yjbuild14
sudo docker run -it -d --name yjbuild14 ubuntu:yj14

sudo docker container exec -it yjbuild14 sh -c 'rm /opt/yujin_ws/gauntlet_2/ -rf'
sudo docker container exec -it yjbuild14 sh -c 'mkdir /opt/yujin_ws/gauntlet_2/build/ -p'
sudo docker container exec -it yjbuild14 sh -c 'rm /opt/yujin_ws/shield_2/ -rf'
sudo docker container exec -it yjbuild14 sh -c 'mkdir /opt/yujin_ws/shield_2/build/ -p'
sudo docker container exec -it yjbuild14 sh -c 'rm /opt/yujin_ws/sword_2/ -rf'
sudo docker container exec -it yjbuild14 sh -c 'mkdir /opt/yujin_ws/sword_2/build/ -p'

echo 'remove old gauntlet, shield and sword in working directory...'
sudo rm ./gauntlet_2 -rf
sudo rm ./shield_2 -rf
sudo rm ./sword_2 -rf
sudo rm *.deb

echo 'copy gauntlet, shield and sword from host into docker image...'
sudo cp -r /opt/yujin_ws/gauntlet_2/ .
sudo cp -r /opt/yujin_ws/shield_2/ .
sudo cp -r /opt/yujin_ws/sword_2/ .

sudo rm ./gauntlet_2/build/ -rf
sudo rm ./shield_2/build/ -rf
sudo rm ./sword_2/build/ -rf

sudo docker cp gauntlet_2/ yjbuild14:/opt/yujin_ws/
sudo docker cp shield_2/ yjbuild14:/opt/yujin_ws/
sudo docker cp sword_2/ yjbuild14:/opt/yujin_ws/

echo 'remove temporal gauntlet, shield and sword in working directory...'
sudo rm ./gauntlet_2 -rf
sudo rm ./shield_2 -rf
sudo rm ./sword_2 -rf

#build gauntlet for shield build
sudo docker container exec -it yjbuild18 sh -c '\
	cd /opt/yujin_ws/gauntlet_2/build/ \
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=FALSE \
	&& make -j12 install'

#build shield for sword build
sudo docker container exec -it yjbuild18 sh -c '\
	ldconfig\
	&& cd /opt/yujin_ws/shield_2/build/\
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=FALSE \
	&& make -j12 install'

#build sword and package
sudo docker container exec -it yjbuild18 sh -c '\
	ldconfig\
	&& cd /opt/yujin_ws/sword_2/build/\
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=TRUE\
	&& make -j12 install\
	&& make package'

# build shield for package
sudo docker container exec -it yjbuild18 sh -c '\
	ldconfig\
	&& cd /opt/yujin_ws/shield_2/build/\
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=TRUE \
	&& make -j12 install \
	&& make package'

# build gauntlet for package
sudo docker container exec -it yjbuild18 sh -c '\
	ldconfig\
	&& cd /opt/yujin_ws/gauntlet_2/build/\
       	&& cmake .. -DBUILD_EXTERNAL_RELEASE=TRUE \
	&& make -j12 install \
	&& make package'

LIB_GAUNTLET_DEB=($(sudo docker container exec -it yjbuild14 sh -c 'ls /opt/yujin_ws/gauntlet_2/build/*.deb'| sed -e 's/\r//g'))
LIB_SHIELD_DEB=($(sudo docker container exec -it yjbuild14 sh -c 'ls /opt/yujin_ws/shield_2/build/*.deb'| sed -e 's/\r//g'))
LIB_SWORD_DEB=($(sudo docker container exec -it yjbuild14 sh -c 'ls /opt/yujin_ws/sword_2/build/*.deb'| sed -e 's/\r//g'))

sudo docker cp yjbuild14:${LIB_GAUNTLET_DEB} .
sudo docker cp yjbuild14:${LIB_SHIELD_DEB} .
sudo docker cp yjbuild14:${LIB_SWORD_DEB} .

sudo chown -c ${USER} *.deb
sudo chmod 644 *.deb

sudo docker container kill yjbuild14
